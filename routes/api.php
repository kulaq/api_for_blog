<?php

use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(PostController::class)->prefix('/posts')->group(function () {
    Route::get('/', 'index')->name('posts.index');
    Route::get('/{post}', 'show')->name('posts.show');
    Route::post('/', 'store')->name('posts.store');
    Route::post('/{post}/comment', 'comment')->name('posts.comment');
    Route::patch('/{post}', 'update')->name('posts.update');
    Route::delete('/{post}', 'delete')->name('posts.delete');
});

Route::controller(UserController::class)->group(function () {
    Route::post('/login', 'login')->name('login');
});

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
