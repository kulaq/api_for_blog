<?php

namespace App\Http\Requests;

use App\Enums\PostStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class PostRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required', 'string'],
            'body' => ['string'],
            'thumbnail' => ['required', 'image'],
            'status' => ['required', new Enum(PostStatus::class)],
            'category_id' => ['required', 'integer']
        ];
    }
}
