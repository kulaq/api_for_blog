<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\NewAccessToken;

class UserController extends Controller
{
    public function login(UserRequest $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if (! Auth::guard('web')->attempt([
            'email' => $email,
            'password' => $password]
        )) {
            return response()->json(['message' => 'неверно введен логин или пароль']);
        }

        $user = Auth::guard('web')->user();

        /** @var NewAccessToken $token */
        $token = $user->createToken('login');

        $user->update(['api_token' => $token]);

        return response()->json(['token' => $token->plainTextToken]);
    }
}
