<?php

namespace App\Http\Controllers;

use App\Enums\PostStatus;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\PostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\Post\MinifiedPostResource;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use function Webmozart\Assert\Tests\StaticAnalysis\integer;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')
        ->only(['index', 'store', 'update', 'delete']);

        $this->middleware('is.published')
            ->only('show');
    }

    public function index()
    {
        $posts = Post::query()
            ->select(['id', 'title', 'thumbnail', 'views', 'created_at'])
            ->whereStatus(PostStatus::Published)
            ->get();

        return MinifiedPostResource::collection($posts);
    }

    public function show(Post $post)
    {
        return new PostResource($post);
    }

    public function store(PostRequest $request)
    {

        $thumbnail = $request->file('thumbnail');
        $path = $thumbnail->storePublicly('thumbnail');


        /** @var Post $post */
        $post = auth()->user()->posts()->create([
            'title' => $request->str('title'),
            'body' => $request->str('body'),
            'thumbnail' => config('app.url') . Storage::url($path),
            'status' => $request->enum('status', PostStatus::class),
            'category_id' => $request->integer('category_id'),
        ]);

        return response()->json([
            'id' => $post->id,
        ], 201);
    }

    public function comment(CommentRequest $request, Post $post)
    {
        return $post->comments()->create([
            'user_id' => auth()->id(),
            'text' => $request->str('text'),
        ])->only('id');
    }

    public function update(UpdatePostRequest $request, Post $post)
    {

        //TODO использовать DTO

        $data = [];

        if ($request->has('title')) {
            $data['title'] = $request->input('title');
        }

        if ($request->has('body')) {
            $data['body'] = $request->input('body');
        }

        if ($request->has('status')) {
            $data['status'] = $request->input('status');
        }

        if ($request->has('category_id')) {
            $data['category_id'] = $request->input('category_id');
        }

        $post->update($data);
    }

    public function delete(Post $post)
    {
        $post->delete();

        return response([
            'status' => 'post deleted',
        ]);
    }
}
